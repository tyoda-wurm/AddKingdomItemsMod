
package org.requiem.mods.kingdomitems;

import com.wurmonline.server.Features;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.behaviours.Actions;
import com.wurmonline.server.behaviours.Seat;
import com.wurmonline.server.behaviours.Vehicle;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import javassist.CtClass;
import javassist.CtPrimitiveType;
import javassist.NotFoundException;
import javassist.bytecode.Descriptor;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.interfaces.Configurable;
import org.gotti.wurmunlimited.modloader.interfaces.ItemTemplatesCreatedListener;
import org.gotti.wurmunlimited.modloader.interfaces.Versioned;
import org.gotti.wurmunlimited.modloader.interfaces.WurmServerMod;
import org.requiem.mods.kingdomitems.items.factories.*;
import org.requiem.mods.kingdomitems.util.SeatsFacadeImpl;
import org.requiem.mods.kingdomitems.util.VehicleFacadeImpl;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Adds various kingdom items and a magic carpet.
 */
public class Initiator implements WurmServerMod, Configurable, ItemTemplatesCreatedListener, Versioned {
    public static final String version = "ty2.2";
    public static final Logger logger = Logger.getLogger(Initiator.class.getName());
    public static boolean wagons = false;
    public static boolean magicCarpets = false;
    public static boolean towers = false;
    public static boolean flags = false;
    public static boolean tents = false;
    public static boolean pavilions = false;
    public static boolean banners = false;
    private static float wagonMaxSpeed = 1.0f;
    private static float wagonMaxDepth = -0.07f;
    private static float wagonMaxHeightDiff = 0.07f;
    public static float wagonDifficulty = 70.0F;
    private static int wagonMaxAllowedLoadDistance = 4;
    private static float wagonSkillNeeded = 21.0f;
    public static int wagonWeightGrams = 240000;
    public static double wagonMinSkill = 40.0d;
    private static float carpetMaxSpeed = 2.0f;
    private static float carpetMaxDepth = 0.07f;
    private static float carpetMaxHeightDiff = 0.07f;
    public static float carpetDifficulty = 80.0f;
    private static float carpetSkillNeeded = 21.0f;
    public static int carpetWeightGrams = 4000;
    public static double carpetMinSkill = 40.0d;

    @Override
    public void configure(Properties properties) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        wagons = Boolean.parseBoolean(properties.getProperty("wagons", String.valueOf(Initiator.wagons)));
        //magicCarpets = Boolean.valueOf(properties.getProperty("magicCarpets", String.valueOf(Initiator.magicCarpets)));
        flags = Boolean.parseBoolean(properties.getProperty("flags", String.valueOf(Initiator.flags)));
        towers = Boolean.parseBoolean(properties.getProperty("towers", String.valueOf(Initiator.towers)));
        tents = Boolean.parseBoolean(properties.getProperty("tents", String.valueOf(Initiator.tents)));
        banners = Boolean.parseBoolean(properties.getProperty("banners", String.valueOf(Initiator.banners)));
        pavilions = Boolean.parseBoolean(properties.getProperty("pavilions", String.valueOf(Initiator.pavilions)));
        wagonMaxSpeed = Float.parseFloat(properties.getProperty("wagonMaxSpeed", String.valueOf(Initiator.wagonMaxSpeed)));
        wagonMaxDepth = Float.parseFloat(properties.getProperty("wagonMaxDepth", String.valueOf(Initiator.wagonMaxDepth)));
        wagonMaxHeightDiff = Float.parseFloat(properties.getProperty("wagonMaxHeightDiff", String.valueOf(Initiator.wagonMaxHeightDiff)));
        wagonDifficulty = Float.parseFloat(properties.getProperty("wagonDifficulty", String.valueOf(Initiator.wagonDifficulty)));
        wagonMaxAllowedLoadDistance = Integer.parseInt(properties.getProperty("wagonMaxAllowedLoadDistance", String.valueOf(Initiator.wagonMaxAllowedLoadDistance)));
        wagonSkillNeeded = Float.parseFloat(properties.getProperty("wagonSkillNeeded", String.valueOf(Initiator.wagonSkillNeeded)));
        wagonMinSkill = Double.parseDouble(properties.getProperty("wagonMinSkill", String.valueOf(Initiator.wagonMinSkill)));
        wagonWeightGrams = Integer.parseInt(properties.getProperty("wagonWeightGrams", String.valueOf(Initiator.wagonWeightGrams)));
        //carpetMaxSpeed = Float.parseFloat(properties.getProperty("carpetMaxSpeed", String.valueOf(Initiator.carpetMaxSpeed)));
        //carpetMaxDepth = Float.parseFloat(properties.getProperty("carpetMaxDepth", String.valueOf(Initiator.carpetMaxDepth)));
        //carpetMaxHeightDiff = Float.parseFloat(properties.getProperty("carpetMaxHeightDiff", String.valueOf(Initiator.carpetMaxHeightDiff)));
        //carpetDifficulty = Float.parseFloat(properties.getProperty("carpetDifficulty", String.valueOf(Initiator.carpetDifficulty)));
        //carpetSkillNeeded = Float.parseFloat(properties.getProperty("carpetSkillNeeded", String.valueOf(Initiator.carpetSkillNeeded)));
        //carpetMinSkill = Double.parseDouble(properties.getProperty("carpetMinSkill", String.valueOf(Initiator.carpetMinSkill)));
        //carpetWeightGrams = Integer.parseInt(properties.getProperty("carpetWeightGrams", String.valueOf(Initiator.carpetWeightGrams)));
        logger.info( "=========================================================================================");
        //if (Initiator.magicCarpets) { debug( "magicCarpets Crafting: Enabled"); }
        //if (!Initiator.magicCarpets) { debug( "magicCarpets Crafting: Disabled"); }
        logger.info( "Wagon Crafting: " + (Initiator.wagons ? "Enabled" : "Disabled"));
        logger.info( "Flag Crafting: " + (Initiator.flags ? "Enabled" : "Disabled"));
        logger.info( "Tower Crafting: " + (Initiator.towers ? "Enabled" : "Disabled"));
        logger.info( "Tent Crafting: " + (Initiator.tents ? "Enabled" : "Disabled"));
        logger.info( "Banner Crafting: " + (Initiator.banners ? "Enabled" : "Disabled"));
        logger.info( "Pavilion Crafting: " + (Initiator.pavilions ? "Enabled" : "Disabled"));
        logger.info( "wagonMaxSpeed: " + wagonMaxSpeed);
        logger.info( "wagonMaxDepth: " + wagonMaxDepth);
        logger.info( "wagonMaxHeightDiff: " + wagonMaxHeightDiff);
        logger.info( "wagonDifficulty: " + wagonDifficulty);
        logger.info( "wagonMaxAllowedLoadDistance: " + wagonMaxAllowedLoadDistance);
        logger.info( "wagonSkillNeeded: " + wagonSkillNeeded);
        logger.info( "wagonMinSkill: " + wagonMinSkill);
        logger.info( "wagonWeightGrams: " + formatter.format(wagonWeightGrams));
        logger.info("Add Kingdom Items Mod Debugging Messages: Enabled");
        logger.info("Add Kingdom Items Mod Debugging Messages: Disabled");
        logger.info( "=========================================================================================");
        registerWagonHook();
        registerWagonManageHook();
        registerCarpetHook();
        //registerCarpetManageHook();
    }

    public static void registerWagonManageHook() {
        try {
            CtClass[] input = {
                    HookManager.getInstance().getClassPool().get("com.wurmonline.server.creatures.Creature"),
                    HookManager.getInstance().getClassPool().get("com.wurmonline.server.items.Item")
            };
            CtClass output = HookManager.getInstance().getClassPool().get("java.util.List");
            HookManager.getInstance().registerHook("com.wurmonline.server.behaviours.VehicleBehaviour", "getVehicleBehaviours",
                    Descriptor.ofMethod(output, input), () -> (proxy, method, args) -> {
                        @SuppressWarnings("unchecked")
                        List<ActionEntry> original = (List<ActionEntry>) method.invoke(proxy, args);
                        Item item = (Item) args[1];
                        Creature performer = (Creature) args[0];
                        if (item.mayManage(performer)) {
                            int itemId = item.getTemplateId();
                            for (int id : WagonFactory.wagonList) {
                                if (id == itemId) {
                                    logger.info("Adding manage permissions");
                                    original.add(Actions.actionEntrys[Actions.MANAGE_WAGON]);
                                }
                            }
                        }
                        if (item.maySeeHistory(performer)) {
                            int itemId = item.getTemplateId();
                            for (int id : WagonFactory.wagonList) {
                                if (id == itemId) {
                                    original.add(new ActionEntry(Actions.SHOW_HISTORY_FOR_OBJECT, "History of Wagon", "viewing"));
                                }
                            }
                        }
                        return original;
                    });
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Failed in permission hook for wagon: ", e);
        }
    }

    public static void registerWagonHook() {
        try {
            CtClass[] input = {
                    HookManager.getInstance().getClassPool().get("com.wurmonline.server.items.Item"),
                    HookManager.getInstance().getClassPool().get("com.wurmonline.server.behaviours.Vehicle")
            };
            CtClass output = CtPrimitiveType.voidType;
            logger.info("Adding vehicle configuration for wagons");
            HookManager.getInstance().registerHook("com.wurmonline.server.behaviours.Vehicles", "setSettingsForVehicle",
                    Descriptor.ofMethod(output, input), () -> (proxy, method, args) -> {
                        Item item = (Item) args[0];
                        int templateId = item.getTemplateId();
                        for (int i : WagonFactory.wagonList) {
                            if (i == templateId) {
                                Vehicle vehicle = (Vehicle) args[1];
                                VehicleFacadeImpl vehfacade = new VehicleFacadeImpl(vehicle);
                                if (Features.Feature.WAGON_PASSENGER.isEnabled()) {
                                    vehfacade.createPassengerSeats(1);
                                } else {
                                    vehfacade.createPassengerSeats(0);
                                }
                                vehfacade.setPilotName("driver");
                                vehfacade.setCreature(false);
                                vehfacade.setEmbarkString("ride");
                                vehfacade.setEmbarksString("rides");
                                vehicle.name = item.getName();
                                vehicle.setSeatFightMod(0, 0.9f, 0.3f);
                                vehicle.setSeatOffset(0, 0.0f, 0.0f, 0.0f, 1.453f);
                                if (Features.Feature.WAGON_PASSENGER.isEnabled()) {
                                    vehicle.setSeatFightMod(1, 1.0f, 0.4f);
                                    vehicle.setSeatOffset(1, 4.05f, 0.0f, 0.84f);
                                }
                                vehicle.maxHeightDiff = wagonMaxHeightDiff;
                                vehicle.maxDepth = wagonMaxDepth;
                                vehicle.skillNeeded = wagonSkillNeeded;
                                vehfacade.setMaxSpeed(wagonMaxSpeed);
                                vehicle.commandType = 2;
                                SeatsFacadeImpl seatfacad = new SeatsFacadeImpl();
                                final Seat[] hitches = {seatfacad.CreateSeat((byte) 2), seatfacad.CreateSeat((byte) 2), seatfacad.CreateSeat((byte) 2), seatfacad.CreateSeat((byte) 2)};
                                hitches[0].offx = -2.0f;
                                hitches[0].offy = -1.0f;
                                hitches[1].offx = -2.0f;
                                hitches[1].offy = 1.0f;
                                hitches[2].offx = -5.0f;
                                hitches[2].offy = -1.0f;
                                hitches[3].offx = -5.0f;
                                hitches[3].offy = 1.0f;
                                vehicle.addHitchSeats(hitches);
                                vehicle.setMaxAllowedLoadDistance(wagonMaxAllowedLoadDistance);
                                return null;
                            }
                        }
                        return method.invoke(proxy, args);
                    });
        } catch (NotFoundException e) {
            logger.log(Level.SEVERE, "Failed in wagon hook: ", e);
        }
    }

    public static void registerCarpetManageHook() {
        try {
            CtClass[] input = {
                    HookManager.getInstance().getClassPool().get("com.wurmonline.server.creatures.Creature"),
                    HookManager.getInstance().getClassPool().get("com.wurmonline.server.items.Item")
            };
            CtClass output = HookManager.getInstance().getClassPool().get("java.util.List");
            HookManager.getInstance().registerHook("com.wurmonline.server.behaviours.VehicleBehaviour", "getVehicleBehaviours",
                    Descriptor.ofMethod(output, input), () -> (proxy, method, args) -> {
                        @SuppressWarnings("unchecked")
                        List<ActionEntry> original = (List<ActionEntry>) method.invoke(proxy, args);
                        Item item = (Item) args[1];
                        Creature performer = (Creature) args[0];
                        if (item.mayManage(performer)) {
                            int itemId = item.getTemplateId();
                            for (int id : MagicCarpetFactory.carpetList) {
                                if (id == itemId) {
                                    logger.info("Adding manage permissions");
                                    original.add(Actions.actionEntrys[Actions.MANAGE_VEHICLE]);
                                    break;
                                }
                            }
                        }
                        if (item.maySeeHistory(performer)) {
                            int itemId = item.getTemplateId();
                            for (int id : MagicCarpetFactory.carpetList) {
                                if (id == itemId) {
                                    original.add(new ActionEntry(Actions.SHOW_HISTORY_FOR_OBJECT, "History of Carpet", "viewing"));
                                    break;
                                }
                            }
                        }
                        return original;
                    });
        } catch (NotFoundException e) {
            logger.log(Level.SEVERE, "failed in permission hook for magic carpet: ", e);
        }
    }

    public static void registerCarpetHook() {
        try {
            CtClass[] input = {
                    HookManager.getInstance().getClassPool().get("com.wurmonline.server.items.Item"),
                    HookManager.getInstance().getClassPool().get("com.wurmonline.server.behaviours.Vehicle")
            };
            CtClass output = CtPrimitiveType.voidType;
            logger.info("Adding vehicle configuration for magic carpets");
            HookManager.getInstance().registerHook("com.wurmonline.server.behaviours.Vehicles", "setSettingsForVehicle",
                    Descriptor.ofMethod(output, input), () -> (proxy, method, args) -> {
                        Item item = (Item) args[0];
                        int templateId = item.getTemplateId();
                        for (int i : MagicCarpetFactory.carpetList) {
                            if (i == templateId) {
                                Vehicle vehicle = (Vehicle) args[1];
                                VehicleFacadeImpl vehfacade = new VehicleFacadeImpl(vehicle);
                                vehfacade.createPassengerSeats(0);
                                vehfacade.setPilotName("rider");
                                vehfacade.setCreature(false);
                                vehfacade.setEmbarkString("ride");
                                vehfacade.setEmbarksString("rides");
                                vehicle.name = item.getName();
                                vehicle.setSeatFightMod(0, 0.9f, 0.3f);
                                vehicle.setSeatOffset(0, 0.0f, 0.0f, 0.0f, 1.453f);
                                vehicle.maxHeightDiff = carpetMaxHeightDiff;
                                vehicle.maxDepth = carpetMaxDepth;
                                vehicle.skillNeeded = carpetSkillNeeded;
                                vehfacade.setMaxSpeed(carpetMaxSpeed);
                                vehicle.commandType = 2;
                                return null;
                            }
                        }
                        return method.invoke(proxy, args);
                    });
        } catch (NotFoundException e) {
            logger.log(Level.SEVERE, "Failed in magic carpet hook: ", e);
        }
    }

    @Override
    public void onItemTemplatesCreated() {
        WagonFactory.addAllWagons();
        TowerFactory.addAllTowers();
        MilitaryTentFactory.addAllTents();
        PavilionFactory.addAllPavilions();
        FlagFactory.addAllFlags();
        BannerFactory.addAllBanners();
        MagicCarpetFactory.addAllCarpets();
    }

    @Override
    public String getVersion() {
        return version;
    }
}
