
package org.requiem.mods.kingdomitems.util;

import com.wurmonline.server.behaviours.Seat;

/**
 * Created by Oluf
 */
public interface SeatsFacade {
	Seat CreateSeat(byte _type);
}
