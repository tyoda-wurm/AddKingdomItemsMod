package org.requiem.mods.kingdomitems;

/**
 * Constants for all the things
 */
public class Constants {

    public static final String[] WAGON_LIST = {
            "model.transports.medium.wagon.jenn", // Jenn-Kellon
            "model.transports.medium.wagon.molr", // Mol-Rehan
            "model.transports.medium.wagon.hots", // Horde of the Summoned
            "model.transports.medium.wagon.zjen", // Dragon Kingdom
            "model.transports.medium.wagon.empi", // Empire of Mol-Rehan
            "model.transports.medium.wagon.blac", // Black Legion
            "model.transports.medium.wagon.ebon", // Ebonaura
            "model.transports.medium.wagon.king", // Kingdom of Sol
            "model.transports.medium.wagon.ther", // The Roman Republic
            "model.transports.medium.wagon.mace", // Macedonia
            "model.transports.medium.wagon.drea", // Dreadnought
            "model.transports.medium.wagon.thec", // The Crusaders
            "model.transports.medium.wagon.pand", // Pandemonium
            "model.transports.medium.wagon.legi", // Legion of Anubis
            "model.transports.medium.wagon.wurm", // Wurm University
            "model.transports.medium.wagon.yval", // Valhalla Descendants
            "model.transports.medium.wagon.apoc", // Apocalypse Order
            "model.transports.medium.wagon.abra", // Abralon
            "model.transports.medium.wagon.comm", // The Commonwealth
            "model.transports.medium.wagon.valh"  // Valhalla
    };

    public static final String[] BANNER_LIST = {
            "model.decoration.banner.jenn", // Jenn-Kellon
            "model.decoration.banner.molr", // Mol-Rehan
            "model.decoration.banner.hots", // Horde of the Summoned
            "model.decoration.banner.zjen", // Dragon Kingdom
            "model.decoration.banner.empi", // Empire of Mol-Rehan
            "model.decoration.banner.blac", // Black Legion
            "model.decoration.banner.ebon", // Ebonaura
            "model.decoration.banner.king", // Kingdom of Sol
            "model.decoration.banner.ther", // The Roman Republic
            "model.decoration.banner.mace", // Macedonia
            "model.decoration.banner.drea", // Dreadnought
            "model.decoration.banner.thec", // The Crusaders
            "model.decoration.banner.pand", // Pandemonium
            "model.decoration.banner.legi", // Legion of Anubis
            "model.decoration.banner.wurm", // Wurm University
            "model.decoration.banner.yval", // Valhalla Descendants
            "model.decoration.banner.apoc", // Apocalypse Order
            "model.decoration.banner.abra", // Abralon
            "model.decoration.banner.comm", // The Commonwealth
            "model.decoration.banner.valh"  // Valhalla
    };

    public static final String[] BANNER_TALL_LIST = {
            "model.decoration.tallbanner.jenn", // Jenn-Kellon
            "model.decoration.tallbanner.molr", // Mol-Rehan
            "model.decoration.tallbanner.hots", // Horde of the Summoned
            "model.decoration.tallbanner.zjen", // Dragon Kingdom
            "model.decoration.tallbanner.empi", // Empire of Mol-Rehan
            "model.decoration.tallbanner.blac", // Black Legion
            "model.decoration.tallbanner.ebon", // Ebonaura
            "model.decoration.tallbanner.king", // Kingdom of Sol
            "model.decoration.tallbanner.ther", // The Roman Republic
            "model.decoration.tallbanner.mace", // Macedonia
            "model.decoration.tallbanner.drea", // Dreadnought
            "model.decoration.tallbanner.thec", // The Crusaders
            "model.decoration.tallbanner.pand", // Pandemonium
            "model.decoration.tallbanner.legi", // Legion of Anubis
            "model.decoration.tallbanner.wurm", // Wurm University
            "model.decoration.tallbanner.yval", // Valhalla Descendants
            "model.decoration.tallbanner.apoc", // Apocalypse Order
            "model.decoration.tallbanner.abra", // Abralon
            "model.decoration.tallbanner.comm", // The Commonwealth
            "model.decoration.tallbanner.valh"  // Valhalla
    };

    public static final String[] FLAG_LIST = {
            "model.decoration.flag.jenn", // Jenn-Kellon
            "model.decoration.flag.molr", // Mol-Rehan
            "model.decoration.flag.hots", // Horde of the Summoned
            "model.decoration.flag.zjen", // Dragon Kingdom
            "model.decoration.flag.empi", // Empire of Mol-Rehan
            "model.decoration.flag.blac", // Black Legion
            "model.decoration.flag.ebon", // Ebonaura
            "model.decoration.flag.king", // Kingdom of Sol
            "model.decoration.flag.ther", // The Roman Republic
            "model.decoration.flag.mace", // Macedonia
            "model.decoration.flag.drea", // Dreadnought
            "model.decoration.flag.thec", // The Crusaders
            "model.decoration.flag.pand", // Pandemonium
            "model.decoration.flag.legi", // Legion of Anubis
            "model.decoration.flag.wurm", // Wurm University
            "model.decoration.flag.yval", // Valhalla Descendants
            "model.decoration.flag.apoc", // Apocalypse Order
            "model.decoration.flag.abra", // Abralon
            "model.decoration.flag.comm", // The Commonwealth
            "model.decoration.flag.valh"  // Valhalla
    };

    public static final String[] TOWER_LIST = {
            "model.structure.guardtower.jenn", // Jenn-Kellon
            "model.structure.guardtower.molr", // Mol-Rehan
            "model.structure.guardtower.hots", // Horde of the Summoned
    };

    public static final String[] TENT_LIST = {
            "model.structure.tent.military.cotton.jenn", // Jenn-Kellon
            "model.structure.tent.military.cotton.molr", // Mol-Rehan
            "model.structure.tent.military.cotton.hots", // Horde of the Summoned
            "model.structure.tent.military.cotton.zjen", // Dragon Kingdom
            "model.structure.tent.military.cotton.empi", // Empire of Mol-Rehan
            "model.structure.tent.military.cotton.blac", // Black Legion
            "model.structure.tent.military.cotton.ebon", // Ebonaura
            "model.structure.tent.military.cotton.king", // Kingdom of Sol
            "model.structure.tent.military.cotton.ther", // The Roman Republic
            "model.structure.tent.military.cotton.mace", // Macedonia
            "model.structure.tent.military.cotton.drea", // Dreadnought
            "model.structure.tent.military.cotton.thec", // The Crusaders
            "model.structure.tent.military.cotton.pand", // Pandemonium
            "model.structure.tent.military.cotton.legi", // Legion of Anubis
            "model.structure.tent.military.cotton.wurm", // Wurm University
            "model.structure.tent.military.cotton.yval", // Valhalla Descendants
            "model.structure.tent.military.cotton.apoc", // Apocalypse Order
            "model.structure.tent.military.cotton.abra", // Abralon
            "model.structure.tent.military.cotton.comm", // The Commonwealth
            "model.structure.tent.military.cotton.valh"  // Valhalla
    };

    public static final String[] PAVILION_LIST = {
            "model.structure.tent.pavilion.cotton.jenn", // Jenn-Kellon
            "model.structure.tent.pavilion.cotton.molr", // Mol-Rehan
            "model.structure.tent.pavilion.cotton.hots", // Horde of the Summoned
    };

    public static final String[] CARPET_LIST = {
            "model.decoration.colorful.carpet.medium"
    };

    public static final String[] NAMES = {
            "Jenn-Kellon", "Mol-Rehan", "Horde of the Summoned", "Dragon Kingdom", "Empire of Mol-Rehan",
            "Black Legion", "Ebonaura", "Kingdom of Sol", "The Roman Republic", "Macedonia",
            "Dreadnought", "The Crusaders", "Pandemonium", "Legion of Anubis", "Wurm University",
            "Valhalla Descendants", "Apocalypse Order",  "Abralon", "The Commonwealth", "Valhalla"
    };

    public static final String[] CARPET_NAMES = {
            "Gliding"
    };

}
