
package org.requiem.mods.kingdomitems.items.factories;

import org.requiem.mods.kingdomitems.Constants;
import org.requiem.mods.kingdomitems.Initiator;
import org.requiem.mods.kingdomitems.items.KingdomPavilion;

import java.util.logging.Level;

/**
 * Creates all kinds of pavilions.
 */
public class PavilionFactory {

    public static void addAllPavilions() {
        for (int i = 0; i < Constants.PAVILION_LIST.length; ++i) {
            if (!Constants.PAVILION_LIST[i].equals("")) {
                int id = KingdomPavilion.addPavilion(Constants.PAVILION_LIST[i], Constants.NAMES[i]);
                if (id == 0) {
                    Initiator.logger.log(Level.WARNING, Constants.NAMES[i] + " pavilion - cant' be created, id is 0");
                }
            }
        }
    }
}
