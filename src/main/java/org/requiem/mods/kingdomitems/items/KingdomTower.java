
package org.requiem.mods.kingdomitems.items;

import com.wurmonline.server.behaviours.BehaviourList;
import com.wurmonline.server.items.*;
import org.gotti.wurmunlimited.modsupport.ItemTemplateBuilder;
import org.requiem.mods.kingdomitems.Initiator;

import java.io.IOException;
import java.util.logging.Level;

/**
 * For creation of towers
 */
public class KingdomTower {

    public static int addTower(String model, String name) {
        Initiator.logger.info("Initiating Kingdom Tower " + model);
        try {
            return createItem(model, name);
        } catch (Exception e) {
            Initiator.logger.log(Level.WARNING, "Initialization of tower failed: " + e);
        }
        return 0;
    }

    private static int createItem(String model, String name) throws IOException {
        ItemTemplateBuilder builder = new ItemTemplateBuilder("org.kingdom.tower." + name);
        builder.name(name + " tower", name + " tower", "A high guard tower.");
        builder.descriptions("excellent", "good", "ok", "poor");
        builder.itemTypes(new short[]{52, 25, 31, 67, 44, 85, 86, 49, 98, 123, 194, 239});
        builder.imageNumber((short) 60);
        builder.combatDamage(0);
        builder.decayTime(19353600L);
        builder.dimensions(400, 400, 600);
        builder.primarySkill(-10);
        builder.modelName(model + ".");
        builder.difficulty(20.0F);
        builder.weightGrams(500000);
        builder.material(Materials.MATERIAL_STONE);
        builder.behaviourType(BehaviourList.itemBehaviour);
        ItemTemplate result = builder.build();
        createCreationEntry(result);
        return result.getTemplateId();
    }

    private static void createCreationEntry(ItemTemplate newTower) {
        if (Initiator.towers) {
            switch(newTower.getName()){
                case "Jenn-Kellon tower":
                    AdvancedCreationEntry jennKellon = CreationEntryCreator.createAdvancedEntry(1013, 132, 492, newTower.getTemplateId(), false, false, 0.0f, true, true, CreationCategories.TOWERS);
                    jennKellon.addRequirement(new CreationRequirement(1, 132, 750, true)); // stone brick
                    jennKellon.addRequirement(new CreationRequirement(2, 492, 750, true)); // mortar
                    jennKellon.addRequirement(new CreationRequirement(3, 22, 100, true)); // plank
                    jennKellon.addRequirement(new CreationRequirement(4, 213, 4, true)); // cloth
                    break;
                case "Mol-Rehan tower":
                    AdvancedCreationEntry molRehan = CreationEntryCreator.createAdvancedEntry(1013, 132, 492, newTower.getTemplateId(), false, false, 0.0f, true, true, CreationCategories.TOWERS);
                    molRehan.addRequirement(new CreationRequirement(1, 132, 750, true)); // stone brick
                    molRehan.addRequirement(new CreationRequirement(2, 492, 750, true)); // mortar
                    molRehan.addRequirement(new CreationRequirement(3, 22, 100, true)); // plank
                    molRehan.addRequirement(new CreationRequirement(4, 213, 4, true)); // cloth
                    molRehan.addRequirement(new CreationRequirement(5, 778, 40, true)); // pottery shingle
                    molRehan.addRequirement(new CreationRequirement(6, 83, 1, true)); // small iron shield
                    molRehan.addRequirement(new CreationRequirement(7, 3, 2, true)); // small axe
                    break;
                case "Horde of the Summoned tower":
                    AdvancedCreationEntry HOTS = CreationEntryCreator.createAdvancedEntry(1013, 1123, 492, newTower.getTemplateId(), false, false, 0.0f, true, true, CreationCategories.TOWERS);
                    HOTS.addRequirement(new CreationRequirement(1, 1123, 500, true)); // slate brick
                    HOTS.addRequirement(new CreationRequirement(2, 492, 500, true)); // mortar
                    HOTS.addRequirement(new CreationRequirement(3, 22, 100, true)); // plank
                    HOTS.addRequirement(new CreationRequirement(4, 213, 4, true)); // cloth
                    HOTS.addRequirement(new CreationRequirement(5, 46, 40, true)); // iron lump
                    break;
                case "Neutral tower":
                    AdvancedCreationEntry neutral = CreationEntryCreator.createAdvancedEntry(1013, 132, 130, newTower.getTemplateId(), false, false, 0.0f, true, true, CreationCategories.TOWERS);
                    neutral.addRequirement(new CreationRequirement(1, 132, 500, true)); // stone brick
                    neutral.addRequirement(new CreationRequirement(2, 130, 500, true)); // clay
                    neutral.addRequirement(new CreationRequirement(3, 22, 100, true)); // plank
                    neutral.addRequirement(new CreationRequirement(4, 213, 4, true)); // cloth
                    break;
                default:
                    AdvancedCreationEntry stoneTower = CreationEntryCreator.createAdvancedEntry(1013, 132, 130, newTower.getTemplateId(), false, false, 0.0f, true, true, CreationCategories.TOWERS);
                    stoneTower.addRequirement(new CreationRequirement(1, 132, 500, true)); // stone brick
                    stoneTower.addRequirement(new CreationRequirement(2, 130, 500, true)); // clay
                    stoneTower.addRequirement(new CreationRequirement(3, 22, 100, true)); // plank
                    break;
            }
        }
    }
}