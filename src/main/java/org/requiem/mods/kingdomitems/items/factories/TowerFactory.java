
package org.requiem.mods.kingdomitems.items.factories;

import org.requiem.mods.kingdomitems.Constants;
import org.requiem.mods.kingdomitems.Initiator;
import org.requiem.mods.kingdomitems.items.KingdomTower;

import java.util.logging.Level;

/**
 * Creates all variants of towers
 */
public class TowerFactory {

    public static void addAllTowers() {
        for (int i = 0; i < Constants.TOWER_LIST.length; ++i) {
            if (!Constants.TOWER_LIST[i].equals("")) {
                int id = KingdomTower.addTower(Constants.TOWER_LIST[i], Constants.NAMES[i]);
                if (id == 0) {
                    Initiator.logger.log(Level.WARNING, Constants.NAMES[i] + " tower - cant' be created, id is 0");
                }
            }
        }

        // add neutral guard tower
        String neutral_model =  "model.structure.neutraltower";
        String neutral_name = "Neutral";
        int id= KingdomTower.addTower(neutral_model, neutral_name);
        if (id == 0) {
            Initiator.logger.log(Level.WARNING, neutral_name + " tower - cant' be created, id is 0");
        }
    }
}
