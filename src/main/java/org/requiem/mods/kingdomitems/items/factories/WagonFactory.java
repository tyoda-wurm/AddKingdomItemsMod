
package org.requiem.mods.kingdomitems.items.factories;

import org.requiem.mods.kingdomitems.items.KingdomWagons;
import org.requiem.mods.kingdomitems.Constants;
import org.requiem.mods.kingdomitems.Initiator;

import java.util.ArrayList;
import java.util.logging.Level;

/**
 * Creates all variants of wagons
 */
public class WagonFactory {

    public static final ArrayList<Integer> wagonList = new ArrayList<>();

    public static void addAllWagons() {
        for (int i = 0; i < Constants.WAGON_LIST.length; ++i) {
            if (!Constants.WAGON_LIST[i].equals("")) {
                int id = KingdomWagons.addWagon(Constants.WAGON_LIST[i], Constants.NAMES[i]);
                if (id != 0) {
                    wagonList.add(id);
                } else {
                    Initiator.logger.log(Level.WARNING, Constants.NAMES[i] + " wagon - cant' be created, id is 0");
                }
            }
        }
    }
}
