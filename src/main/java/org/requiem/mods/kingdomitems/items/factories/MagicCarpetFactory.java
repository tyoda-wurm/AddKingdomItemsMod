
package org.requiem.mods.kingdomitems.items.factories;

import org.requiem.mods.kingdomitems.Initiator;
import org.requiem.mods.kingdomitems.items.MagicCarpets;
import org.requiem.mods.kingdomitems.Constants;

import java.util.ArrayList;
import java.util.logging.Level;

/**
 * Creates all variants of magic carpets
 */
public class MagicCarpetFactory {

    public static final ArrayList<Integer> carpetList = new ArrayList<>();

    public static void addAllCarpets() {
        for (int i = 0; i < Constants.CARPET_LIST.length; ++i) {
            if (!Constants.CARPET_LIST[i].equals("")) {
                int id = MagicCarpets.addCarpet(Constants.CARPET_LIST[i], Constants.CARPET_NAMES[i]);
                if (id != 0) {
                    carpetList.add(id);
                } else {
                    Initiator.logger.log(Level.WARNING, Constants.CARPET_NAMES[i] + " carpet - cant' be created, id is 0");
                }
            }
        }
    }
}

