
package org.requiem.mods.kingdomitems.items;

import com.wurmonline.server.behaviours.BehaviourList;
import com.wurmonline.server.items.*;
import org.gotti.wurmunlimited.modsupport.ItemTemplateBuilder;
import org.requiem.mods.kingdomitems.Initiator;

import java.io.IOException;
import java.util.logging.Level;

/**
 * For creation of flags
 */
public class KingdomFlag {

    public static int addFlag(String model, String name) {
        Initiator.logger.info("Initiating Kingdom Flag " + model);
        try {
            return createItem(model, name);
        } catch (Exception e) {
            Initiator.logger.log(Level.WARNING, "Initialization of wagon failed: " + e);
        }
        return 0;
    }

    private static int createItem(String model, String name) throws IOException {
        ItemTemplateBuilder builder = new ItemTemplateBuilder("org.kingdom.flag." + name);
        builder.name(name + " flag", name + " flags", "A symbol of the " + name + " kingdom.");
        builder.descriptions("excellent", "good", "ok", "poor");
        builder.itemTypes(new short[]{
                ItemTypes.ITEM_TYPE_CLOTH,
                ItemTypes.ITEM_TYPE_TURNABLE,
                ItemTypes.ITEM_TYPE_COLORABLE,
                ItemTypes.ITEM_TYPE_WIND,
                ItemTypes.ITEM_TYPE_MISSION,
                ItemTypes.ITEM_TYPE_DECORATION,
                ItemTypes.ITEM_TYPE_FOUR_PER_TILE,
                ItemTypes.ITEM_TYPE_HASDATA,
                ItemTypes.ITEM_TYPE_DESTROYABLE,
                ItemTypes.ITEM_TYPE_IMPROVEITEM,
                ItemTypes.ITEM_TYPE_REPAIRABLE,
                ItemTypes.ITEM_TYPE_PLANTABLE,
                ItemTypes.ITEM_TYPE_IMPROVE_USES_TYPE_AS_MATERIAL
        });
        builder.imageNumber((short) 640);
        builder.combatDamage(0);
        builder.decayTime(9072000L);
        builder.dimensions(5, 5, 205);
        builder.primarySkill(-10);
        builder.modelName(model + ".");
        builder.difficulty(40.0f);
        builder.weightGrams(2500);
        builder.material(Materials.MATERIAL_WOOD_BIRCH);
        builder.value(10000);
        builder.isTraded(true);
        builder.behaviourType(BehaviourList.itemBehaviour);
        ItemTemplate result = builder.build();
        createCreationEntry(result);
        return result.getTemplateId();
    }

    private static void createCreationEntry(ItemTemplate newBanner) {
        if (Initiator.flags) {
            CreationEntryCreator.createSimpleEntry(10016, 213, 23, newBanner.getTemplateId(), true, true, 0.0F, false, false, CreationCategories.FLAGS);
        }
    }
}