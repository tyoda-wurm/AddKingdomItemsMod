
package org.requiem.mods.kingdomitems.items.factories;

import org.requiem.mods.kingdomitems.items.KingdomMilitaryTent;
import org.requiem.mods.kingdomitems.Constants;
import org.requiem.mods.kingdomitems.Initiator;

import java.util.logging.Level;

/**
 * Creates all kinds of tents.
 */
public class MilitaryTentFactory {

    public static void addAllTents() {
        for (int i = 0; i < Constants.TENT_LIST.length; ++i) {
            if (!Constants.TENT_LIST[i].equals("")) {
                int id = KingdomMilitaryTent.addTent(Constants.TENT_LIST[i], Constants.NAMES[i]);
                if (id == 0) {
                    Initiator.logger.log(Level.WARNING, Constants.NAMES[i] + " tent - cant' be created, id is 0");
                }
            }
        }
    }
}
