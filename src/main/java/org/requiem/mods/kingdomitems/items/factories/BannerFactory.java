
package org.requiem.mods.kingdomitems.items.factories;

import org.requiem.mods.kingdomitems.items.KingdomBanner;
import org.requiem.mods.kingdomitems.Constants;
import org.requiem.mods.kingdomitems.Initiator;

import java.util.logging.Level;

/**
 * Creates all kinds of Banners.
 */
public class BannerFactory {

    public static void addAllBanners() {
        for (int i = 0; i < Constants.BANNER_LIST.length; ++i) {
            if (!Constants.BANNER_LIST[i].equals("")) {
                int id = KingdomBanner.addBanner(Constants.BANNER_LIST[i], Constants.NAMES[i], false);
                if (id == 0) {
                    Initiator.logger.log(Level.WARNING, Constants.NAMES[i] + " banner - cant' be created, id is 0");
                }
            }
            if (!Constants.BANNER_TALL_LIST[i].equals("")) {
                int id = KingdomBanner.addBanner(Constants.BANNER_TALL_LIST[i], Constants.NAMES[i], true);
                if (id == 0) {
                    Initiator.logger.log(Level.WARNING, Constants.NAMES[i] + " tall banner - cant' be created, id is 0");
                }
            }
        }
    }
}
