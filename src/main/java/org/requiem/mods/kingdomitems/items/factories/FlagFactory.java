
package org.requiem.mods.kingdomitems.items.factories;

import org.requiem.mods.kingdomitems.items.KingdomFlag;
import org.requiem.mods.kingdomitems.Constants;
import org.requiem.mods.kingdomitems.Initiator;

import java.util.logging.Level;

/**
 * Creates every flag.
 */
public class FlagFactory {

    public static void addAllFlags() {
        for (int i = 0; i < Constants.FLAG_LIST.length; ++i) {
            if (!Constants.FLAG_LIST[i].equals("")) {
                int id = KingdomFlag.addFlag(Constants.FLAG_LIST[i], Constants.NAMES[i]);
                if (id == 0) {
                    Initiator.logger.log(Level.WARNING, Constants.NAMES[i] + " flag - cant' be created, id is 0");
                }
            }
        }
    }
}
