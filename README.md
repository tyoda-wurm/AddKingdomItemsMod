# About this fork
This is a fork of Jubaroo's mod. We've added many "missing" types of items. Berkle and Olette created these textures from scratch.

The mod now requires serverpacks.

# Add Kingdom Items Mod V1.7

- Enables the creation of PMK wagons, banners, and flags not normally able to be crafted.
- Unzip this archive into the `mods` folder. Serverpacks is required on server side!
- See AddKingdomItemsMod.config for vehicle configuration settings

**List of PMKs**
1. Jenn-Kellon
2. Mol-Rehan
3. Horde of the Summoned
4. Dragon Kingdom
5. Empire of Mol-Rehan
6. Black Legion
7. Ebonaura
8. Kingdom of Sol
9. The Roman Republic
10. Macedonia
11. Dreadnought
12. The Crusaders
13. Pandemonium
14. Legion of Anubis
15. Wurm University
16. Valhalla Descendants
17. Apocalypse Order
18. Abralon
19. The Commonwealth
20. Valhalla

**Version ty2.2**
- Added back Valhalla kingdom (previously removed by accident)
- Fixed Valhalla Descendants tent looking like Valhalla tent


**Version ty2.1**
- Added animation to flags, banners wagons
- Added unfinished models to tents, pavilions, wagons
- Fixed wagon driving visibility issue

**Version ty2.0**
- Added The Commonwealth Kingdom with all items
- Added the neutral guard tower
- Added Abralon, Crusaders banner, flag, wagon
- Added Abralon, Crusaders, Kingdom of Sol tall banner
- Added 11 new military tents

**Version 1.7** 

- Updated for 1.8.0.3
- Added new PMKs

**Version 1.6** 

- Fixed loom being used when making the magic carpet.
- Made wagons able to be dyed.

**Version 1.5** 

- Fixed naming issue that cause it to be incompatible with some servers.

**Version 1.4** 

New Vehicle - Magic Carpet  
- Enable the creation of a magic carpet that can be picked up and placed at any time. They cannot go over water or hold any cargo.  
- Enabled configuration options for the wagons and carpets.  
- Added Legion of Anubis PMK stuff  

 **Version 1.3** 
- Updated to add container size to pavilions and tents. Fixed wagon speed and slope height to match WU default setting. 
- Added the ability to set the maximum population of each drake type. Updated the properties file to include notes on what the settings do. 

 **Version 1.2** 
 
- Updated to add pavilions, military tents, and guard towers from the existing kingdoms.  
- Added MAX_HEIGHT_DIFF option to vehicle settings. Added option to set ghost drake model. Fixed ghost drake showing up as a guide. Fixed drakes not being able to be tamed.  

 **Version 1.1** 

- Added all of the WO player made kingdoms that have textures contained in the default texture packs that come with the game.  
- Enables customizations that were previously unavailable.  

 **Version 1.0** 
 
- Enables the creation of PMK wagons, banners, and flags not normally able to be crafted.  
- Allows breedable, rideable, tameable Drakes to be enabled in Wurm. Many Drake settings can be customized to fit your needs.  
